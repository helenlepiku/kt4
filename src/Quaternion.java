import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

   private double a;
   private double b;
   private double c;
   private double d;
   private static final double z = 0.0000000001;
   private static final double hashConstant = 123;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() { return a; }

   /** Imaginary part i of the quaternion.
    * @return imaginary part i
    */
   public double getIpart() {
      return b;
   }

   /** Imaginary part j of the quaternion.
    * @return imaginary part j
    */
   public double getJpart() { return c; }

   /** Imaginary part k of the quaternion.
    * @return imaginary part k
    */
   public double getKpart() {
      return d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      double[] parts = new double[] {a, b, c, d};
      char[] partChars = new char[] {Character.MIN_VALUE, 'i', 'j', 'k'};

      StringBuilder elems = new StringBuilder();
      for (int i = 0; i < parts.length; i++) {
         if (i == 0) {
            elems.append(parts[i]);
         } else if (parts[i] < 0) {
            elems.append(' ').append('-').append(' ');
            elems.append(Math.abs(parts[i]));
            elems.append(partChars[i]);
         } else {
            elems.append(' ').append('+').append(' ');
            elems.append(parts[i]);
            elems.append(partChars[i]);
         }
      }
      return elems.toString();
   }

   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      s = s.trim();
      if (s.isEmpty()) {
         throw new RuntimeException("Quaternion is empty!");
      }

      if (!checkString(s)) {
         throw new RuntimeException("Invalid Quaternion part label in quaternion string value " + s);
      }

      String reversed = new StringBuilder(s).reverse().toString();
      if (reversed.substring(0, 1).equals("-") || reversed.substring(0, 1).equals("+")) {
         throw new RuntimeException("Invalid symbol - in quaternion " + s);
      }

      String[] arr = s.split(" ");
      double[] d = new double[] {0.0, 0.0, 0.0, 0.0};
      int index = 0;
      boolean isMinus = false;
      int minusIndex = 0;


      StringBuilder negativePart = new StringBuilder();

      for (int i = 0; i < arr.length; i++) {
         String str = arr[i].trim();
         if (str.equals("-")) {
            isMinus = true;
            negativePart.append('-');
            minusIndex++;
            continue;
         }

         StringBuilder qPart = new StringBuilder();
         for (int j = 0; j < str.length(); j++) {

            char c = str.toCharArray()[j];

            if (c == '.' || c == '-' || Character.isDigit(c)) {
               if (isMinus) {
                  negativePart.append(c);
               } else {
                  qPart.append(c);
               }
            } else if (!(c == '+' || c == 'i' || c == 'j' || c == 'k' || c == ' ')){
               throw new IllegalArgumentException("Invalid character " + c + " in quaternion " + s);
            }
         }
         if (isMinus && minusIndex == 0) {
            minusIndex++;
         } else if (isMinus && (minusIndex > 0)) {
            d[index] = Double.parseDouble(negativePart.toString());
            isMinus = false;
            negativePart = new StringBuilder();
            minusIndex = 0;
            index++;
         } else if (!qPart.toString().isEmpty()) {
            d[index] = Double.parseDouble(qPart.toString());
            index++;
         }
      }
      double newR = d[0];

      double newI;
      if (checkPart(s, 'i')) {
         newI = d[1];
      } else {
         newI = 0.0;
      }

      double newJ;
      if (checkPart(s, 'j')) {
         newJ = d[2];
      } else {
         newJ = 0.0;
      }

      double newK;
      if (checkPart(s, 'k')) {
         newK = d[3];
      } else {
         newK = 0.0;
      }

      if ((!checkPart(s, 'j') && checkPart(s, 'k')) || (!checkPart(s, 'i') && checkPart(s, 'j'))) {
         throw new RuntimeException("Invalid quaternion string " + s);
      }

      return new Quaternion(newR, newI, newJ, newK);


      //return new Quaternion(d[0], d[1], d[2], d[3]);
   }


   public static boolean checkString(String str) {
      char i = 'i';
      boolean iOk = false;
      int iCount = 0;

      char j = 'j';
      boolean jOk = false;
      int jCount = 0;

      char k = 'k';
      boolean kOk = false;
      int kCount = 0;

      for (char c : str.toCharArray())
         if (c == i){
            iCount++;
         } else if (c == j) {
            jCount++;
         } else if (c == k) {
            kCount++;
         }
      return iCount <= 1 && jCount <= 1 && kCount <= 1;
   }


   public static boolean checkPart(String str, char elem) {
      int kCount = 0;

      for (char c : str.toCharArray())
         if (c == elem){
            kCount++;
         }
      return kCount >= 1;
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(a, b, c, d);
   }

   /** Test whether the quaternion is zero.
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return Math.abs(a) < z && Math.abs(b) < z && Math.abs(c) < z && Math.abs(d) < z;
   }

   /** Conjugate of the quaternion. Expressed by the formula
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {

      return new Quaternion(a, -b, -c, -d);
   }

   /** Opposite of the quaternion. Expressed by the formula
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {

      return new Quaternion(-a, -b, -c, -d);
   }

   /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {

      return new Quaternion((a + q.getRpart()), (b + q.getIpart()), (c + q.getJpart()), (d + q.getKpart()));
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {

      //RIJK
      //(a1a2-b1b2-c1c2-d1d2)
      double newR = a * q.getRpart() - b * q.getIpart() - c * q.getJpart() - d * q.getKpart();

      //IRKJ
      //(a1b2+b1a2+c1d2-d1c2)i
      double newI = a * q.getIpart() + b * q.getRpart() + c * q.getKpart() - d * q.getJpart();

      //JKRI
      //(a1c2-b1d2+c1a2+d1b2)j
      double newJ = a * q.getJpart() - b * q.getKpart() + c * q.getRpart() + d * q.getIpart();

      //KJIR
      //(a1d2+b1c2-c1b2+d1a2)k
      double newK = a * q.getKpart() + b * q.getJpart() - c * q.getIpart() + d * q.getRpart();

      return new Quaternion(newR, newI, newJ, newK);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {

      return new Quaternion(a * r, b * r, c * r, d * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if ((a*a+b*b+c*c+d*d) < z) {
         throw new IllegalArgumentException("Warning! Division by zero not allowed! " +
                 "Error occurred while inverting the Quaternion because all Quaternion parts equal zero.");
      }
      return new Quaternion(a/(a*a+b*b+c*c+d*d), (-b)/(a*a+b*b+c*c+d*d), (-c)/(a*a+b*b+c*c+d*d), (-d)/(a*a+b*b+c*c+d*d));
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {

      return new Quaternion((a - q.getRpart()), (b - q.getIpart()), (c - q.getJpart()), (d - q.getKpart()));

   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {

      try{
         Quaternion inverseQ = q.inverse();

         return times(inverseQ);

      } catch (IllegalArgumentException e) {

         throw new RuntimeException("Division by zero not allowed! Error occurred while inverting the Quaternion " + q
                 + " because all Quaternion " + q + " parts equal to zero.");
      }
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {

      try {
         Quaternion inverseQ = q.inverse();

         return inverseQ.times(this);

      } catch (IllegalArgumentException e) {

         throw new RuntimeException("Division by zero not allowed! Error occurred while inverting the Quaternion " + q
                 + " because all Quaternion " + q + " parts equal to zero.");
      }
   }

   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {

      if (!(qo instanceof Quaternion)) {
         return false;
      }
      return minus((Quaternion) qo).isZero();
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {

      Quaternion res1 = this.times(q.conjugate());
      Quaternion res2 = q.times(conjugate());
      Quaternion res3 = res1.plus(res2);

      return res3.times(0.5);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {

      return Arrays.hashCode(new double[] {this.getRpart(), this.getIpart(), this.getJpart(), this.getKpart()});

   }

   /** Norm of the quaternion. Expressed by the formula
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(a*a+b*b+c*c+d*d);
   }

   /** Main method for testing purposes.
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      /*Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: "
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
      System.out.println("-0.5 + 0.7i - 7.0j + 5.5k");
      System.out.println(valueOf("-0.5 + 0.7i - 7.0j + 5.5k"));*/

      /*Quaternion test = new Quaternion(0,-0.1,0,0);
      Quaternion testA = new Quaternion(0,-0.1,0,0);
      System.out.println(test.isZero());
      System.out.println(test.toString());
      System.out.println(valueOf(test.toString()));*/
      //System.out.println(checkString("-0.6 - 0.1i - 0.7i + 0.8k"));

      //String test2 = "-0.6 - 0.1i - 0.7j + 0.8k";
      System.out.println(valueOf("3 -3j"));
   }
}
// end of file